#!/bin/bash
name="kde-plugin"
version=`cat kde-plugin/contents/scripts/VERSION | tr -d '\n' | tr -d ' '`
echo "Packaging $version"
find . -type d -iname "__pycache__" -exec rm -rfv {} \;
kpackagetool5 -r $name; kpackagetool5 -i $name &&
    tar -czvf makeThatPape_v$version.tar.gz --exclude='disabled_*.py' kde-plugin
kbuildsycoca5