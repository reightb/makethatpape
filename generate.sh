#!/bin/bash

path=$1
command="python $path/main.py -g triangles"

res=(
    "1920x1080"
    "800x600"
    "3840x2160"
)

echo Setting up using path $path

work=()
for i in {0..100}; do
    ra=$((( RANDOM % 300 ) + 50))
    col="#`openssl rand -hex 3`"
    for r in "${res[@]}"; do
        #work+=("$command -o triangles_${i}_$r.png|")
        $command -o triangles_${i}_${r}_${ra}.png -p radius=$ra -p color=$col &
    done
done

export IFS="|"
for w in ${work[@]}; do
    $w &
done

$work | parallel --dryrun
#echo ${work[@]}
#parallel -n 10 --dryrun