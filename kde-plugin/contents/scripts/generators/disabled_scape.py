 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
import random
from utils import color_util, util
import operator, math

def draw_hex(draw, x, y, r, color):
    r2 = r/2
    pts = [
        (x-r2, y-r),
        (x+r2, y-r),
        (x+r, y),
        (x+r2, y+r),
        (x-r2, y+r),
        (x-r, y)
    ]
    draw.polygon(pts, fill=color, outline="black")

class scape:
    def print_options(self):
        return [
            util.opt("color1", "color", "random"),
            util.opt("color2", "color", "random"),
            util.opt("color3", "color", "random"),
            util.opt("num-colors", int, 10, sane_low=5, sane_high=30),
            util.opt("radius", int, 100, sane_low=20, sane_high=300),
            util.opt("echoes", int, 5, sane_low=2, sane_high=7),
        ]
        

    def generate(self, image, options):
        w, h = image.size

        draw = ImageDraw.Draw(image)
        colors = []

        image.paste(self.color1, [0, 0, w, h])
        pix = image.load()
        for _ in range(self.num_colors):
            colors.append(color_util.random_close(color_util.hex_to_rgb(self.color1)))
        colors = [self.color1, self.color2, self.color3]
        r = self.radius
        r = random.randrange(200, 700)
        r2 = r/2
        offset = math.sqrt(r**2-r2**2)
        a60 = math.radians(60)
        print(offset)
        for y in range(0, int(h/r)):
            for x in range(0, int(w/r)):
                for i in range(2):
                    if x % 2 == 0:
                        draw_hex(draw, (r*x*2), y*2*r, r, colors[i%len(colors)])
                    else:
                        draw_hex(draw, (r*x*2)+offset*math.cos(a60), y*2*r+offset*math.sin(a60), r, self.color2)
     
        return image