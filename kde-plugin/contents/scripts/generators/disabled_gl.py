#!

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL.shaders import *

from PIL import Image
from PIL import ImageOps

import os, sys

import numpy
from generators import circles
from utils import util, color_util

class gl:
    program = None
    image = None
    w = 0
    h = 0
    def read(self, file):
        content = open(os.path.join(os.path.dirname(__file__), file), 'r').read()
        return content

    def init(self, w, h):
        glutInit()
        glutInitDisplayMode(GLUT_RGBA)
        glutInitWindowSize(w, h)  
        glutInitWindowPosition(0, 0)  
        wind = glutCreateWindow("nothing")
        glutHideWindow()

    def load_program(self):
            
        vertexShader = compileShader(self.read("gl/bloom.vert"), GL_VERTEX_SHADER)
        fragmentShader = compileShader(self.read("gl/bloom.frag"), GL_FRAGMENT_SHADER)

        self.program = glCreateProgram()
        glAttachShader(self.program, vertexShader)
        glAttachShader(self.program, fragmentShader)
        glLinkProgram(self.program)
  
    def img_to_tex(self, image):
        img_data = numpy.array(list(image.getdata()), numpy.uint8)

        texId = glGenTextures(1)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glBindTexture(GL_TEXTURE_2D, texId)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.size[0], image.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, img_data)
        return texId

    def generate(self, image, options):
        self.w, self.h = image.size
        self.image = image

        cc = circles.circles()
        w = self.w
        h = self.h
        options = {"color":color_util.random_hex()}
        util.set_options_on_obj(cc, options)
        image = cc.generate(image, options)

        self.init(w, h)
        self.load_program()
        #d = image.tobytes("raw", "RGBX", 0, -1)
        #print(d)
        texId = self.img_to_tex(image)

        glViewport(0, 0, w, h)
        glClearDepth(1) # just for completeness
        glClearColor(1.0,0,0,0)
        glClear(GL_COLOR_BUFFER_BIT)

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        #glUseProgram(self.program)
      
      
        glEnable(GL_TEXTURE_2D)
        glBindTexture(GL_TEXTURE_2D, texId) 
        
        glutSwapBuffers()
        final_data = glReadPixels(0, 0, w, h, GL_RGBA, GL_UNSIGNED_BYTE)
        image = Image.frombytes("RGBA", (w, h), final_data)
        image = ImageOps.flip(image) 
        return image