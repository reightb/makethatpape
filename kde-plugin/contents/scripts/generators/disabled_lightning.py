 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
import random
from utils import color_util, util
import operator, math

class lightning:
    def print_options(self):
        return [
            util.opt("color", "color", "random"),
            util.opt("num-lightning", int, 5, sane_low=5, sane_high=30),
            util.opt("rectangles", int, 3, sane_low=1, sane_high=3),
            util.opt("num-colors", int, 10, sane_low=4, sane_high=20),
        ]
        
    def light(self, image, x, y, iterations):
        pass

    def generate(self, image, options):
        w, h = image.size

        draw = ImageDraw.Draw(image)
        self.color = color_util.hex_to_rgb(self.color)
        colors = []

        image.paste(self.color, [0, 0, w, h])
        pix = image.load()
        for _ in range(self.num_colors):
            colors.append(color_util.random_close(self.color))
       

        avg_rec = int(w/self.rectangles)
        print(avg_rec)
        weights = []
        cur_weight = self.rectangles
        for i in range(self.rectangles):
            value = random.uniform(0.8, 1.2)
            if cur_weight - value < 0:
                value = -(cur_weight-value)
            cur_weight -= value
            if cur_weight > 0 and i == self.rectangles-1:
                value += cur_weight
            weights.append(value)
        random.shuffle(weights)
        print(weights, sum(weights))
        cx = 0
        cy = 0
        for r in weights:
            w = r*avg_rec
            draw.rectangle((cx, cy, cx+w, cy+h), fill=random.choice(colors), width=5)
            cx += w
        return image