 
#!/usr/bin/python
from PIL import Image, ImageDraw
import click
from utils import color_util, util

class noise:
    def print_options(self):
        return [
            util.opt("block-size", int, 4)
        ]
        

    def generate(self, image, options):
        w, h = image.size

        draw = ImageDraw.Draw(image)
        
        bs = self.block_size
        for y in range(int(h/bs)):
            for x in range(int(w/bs)):
                if x+y%2 == 0:
                    c = (0, 0, 0)
                else:
                    c = (255, 255, 0)
                c = color_util.random_rgb()
                p1 = (x*bs, y*bs)
                p2 = (bs*x+bs, bs*y+bs)
                draw.rectangle([p1, p2], outline=c, fill=c)

        return image