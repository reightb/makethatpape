# MakeThatPape
![squares](images/squares.png)

MakeThatPape is a small python utility used to generate wallpapers.
It is currently integrated with KDE but can easily be called from the CLI and therefore integrated just about anywhere and on any platform.
##### Table of Contents  
* [Installation](#installation)
  * [Dependencies](#dependencies)
  * [Bare bones](#bare-bones)
  * [KDE](#kde-integration)
* [Generators](#Generators)
* [Development](#Development)
## Installation
### Dependencies
To get anything working, make sure you have the following installed:
* python3
* pillow (via pip)
* click (via pip)


On debian based distros, that would be something like:

`sudo apt install python-pillow python-click`

And on arch/other distros:

`sudo pip install click pillow`

*Note: on Debian based distros you might also need to install qml-module-qt-labs-platform*

### Bare bones
If you just want to get the application working on any platform, just git clone this repository and navigate to:
`kde-plugin/contents/scripts`
Once there, you may get help using the following command:
`./main.py -h`

When you want to start generating wallpapers, you may do so using the following command:
`./main.py -g lines -s 1920x1080`
Where `1920x1080` is the desired resolution, and `lines` is the generator of your choice, see [generators](#generators) for a list of currently available generators.

An example workflow of this is as follows:
```
git clone https://gitlab.com/reightb/makethatpape
cd makethatpape/kde-plugin/contents/scripts/
./main.py -g lines -p line-min-size=10 -p line-max-size=20 -o hello.png
```
### KDE integration
If you want to install this via KDE, I would recommend doing so from the [KDE Store](https://store.kde.org/p/1335518/) and looking for "MakeThatPape generator" and you can easily install it from there. If you've read this and still want to install it manually; heres how:
```
git clone https://gitlab.com/reightb/makethatpape
cd makethatpape/kde-plugin/
rm -rf ~/.local/share/plasma/wallpapers/com.gitlab.reightb.makethatpape; ./package.sh
```
## Generators
Here is the currently supported list of generators, along with examples of each of those.
In order to stay up to date on each generator's options, they will be omitted from this documentation; either look at the generator's `print_options()` function or invoke `./main.py -j` to get a json dump of all the options available.
### Triangles
![triangles](images/triangles.png)
### Circles
![circles](images/circles.png)
### Lines
![lines](images/lines.png)
### Gradient
![gradient](images/gradient.png)
### Grid
![grid](images/grid.png)
### Squares
![squares](images/squares.png)
## Development
### Dos and donts
There is very little that you could do that would displease me in terms of contributions.
If you have pull requests or ideas for generators, make sure to let me know in the form of an issue.
If you have created a generator yourself, make a pull request and I'll likely accept it!
### Extending the generators
Each wallpaper generator is a file inside scripts/generators. Each of those files expect a class name matching the file name, a function called `generate(self, image, options)` and possibly a function called `print_options(self)`(see scripts/generators for examples).

If you'd like to create a generator, the workflow would be something like this:
* copy/create a generator and name it `something.py`
* rename the class in `something.py` to match the name of the file (i.e. `something`)
* populate `print_options()` and `generate()` as needed
* make sure `generate()` populates + returns an image

... and done - assuming you've actually put something interesting in the `generate()` step :)

If you're trying to extend the kde plugin, here are the commands I used whilst developing:
#### Reinstalling the wallpaper plugin
`rm -rf ~/.local/share/plasma/wallpapers/com.gitlab.reightb.makethatpape; ./package.sh`

#### Previewing the QML/checking for errors
`qmlscene kde-plugin/contents/ui/config.qml`
